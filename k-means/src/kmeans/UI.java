package kmeans;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * 
 * UI for k-means demo.
 * 
 * @author Antti Pasanen
 *
 */

public class UI  extends Application {

	private double xNormalized[];
	private double yNormalized[];
	private static int RANGE = 500; // canvas size
	private Kmeans kmeans;
	private String[] attributes;
	private ChoiceBox<String> xAttributeChoiceBox;
	private ChoiceBox<String> yAttributeChoiceBox;
	private ChoiceBox<Integer> kChoiceBox;
	private List<CSVRecord> records; // data read from csv file
	private GraphicsContext gc;
	private Canvas canvas;
	private FileChooser fileChooser;
	private Label textField;
	
	/**
	 * Lift-off!
	 * 
	 * @param args arghs
	 */
	public static void main(String[] args) {
		launch(args);
    }

	/**
	 * 
	 *  UI generation.
	 */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("2D k-means demo, canvas size " + RANGE + "x" + RANGE);
        primaryStage.setResizable(false);
        
        // left side of UI, canvas
        canvas = new Canvas(RANGE, RANGE);
        gc = canvas.getGraphicsContext2D();
        HBox leftSideBox = new HBox();
        leftSideBox.getChildren().add(canvas);
        leftSideBox.setMaxSize(RANGE, RANGE);
        leftSideBox.setStyle("-fx-border-style: solid inside;" + 
                "-fx-border-width: 2;" +
                "-fx-border-insets: 5;" + 
                "-fx-border-color: black;");

        // load & save
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV file", "*.csv"));

        // right side buttons
        textField = textField();
        kChoiceBox = kChoiceBox();
        xAttributeChoiceBox = attributeChoiceBox();
        yAttributeChoiceBox = attributeChoiceBox();
        Label xLabel = createLabel("X: ");
        Label yLabel = createLabel("Y: ");
        Label kLabel = createLabel("K: ");
        Button fileSaveButton = fileSaveButton(primaryStage);
        Button fileLoadButton = fileLoadButton(primaryStage);
        Button centroidButton = centroidButton();
        Button clusterButton = clusterButton();
        
        // pair some buttons
        HBox fileAccessGroup = buttonGroup();
        fileAccessGroup.getChildren().add(fileLoadButton);
        fileAccessGroup.getChildren().add(fileSaveButton);
        HBox kChoiceBoxGroup = buttonGroup();
        kChoiceBoxGroup.getChildren().add(kLabel);
        kChoiceBoxGroup.getChildren().add(kChoiceBox);
        HBox xChoiceBoxGroup = buttonGroup();
        xChoiceBoxGroup.getChildren().add(xLabel);
        xChoiceBoxGroup.getChildren().add(xAttributeChoiceBox);
        HBox yChoiceBoxGroup = buttonGroup();
        yChoiceBoxGroup.getChildren().add(yLabel);
        yChoiceBoxGroup.getChildren().add(yAttributeChoiceBox);
        
        // right side of UI, controls
        VBox rightSideBox = new VBox();
        rightSideBox.setSpacing(10);
        rightSideBox.getChildren().add(fileAccessGroup);
        rightSideBox.getChildren().add(textField);
        rightSideBox.getChildren().add(xChoiceBoxGroup);
        rightSideBox.getChildren().add(yChoiceBoxGroup);
        rightSideBox.getChildren().add(kChoiceBoxGroup);
        rightSideBox.getChildren().add(centroidButton);
        rightSideBox.getChildren().add(clusterButton);
        
        // UI root-box
        HBox root = new HBox();
        root.getChildren().add(leftSideBox);
        root.getChildren().add(rightSideBox);
        
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
    
    /**
     * Creates a Label with a border.
     * 
     * @return Label
     */
    private Label textField() {
        Label tField = new Label();
        tField.setPrefWidth(250);
        tField.setStyle("-fx-border-style: solid inside;" + 
                "-fx-border-width: 1;" + 
        		"-fx-border-color: black;");
        
        return tField;
    }
    
    
    /**
     * Creates a Label with given text.
     * 
     * @param text Text on the label
     * @return Label
     */
    private Label createLabel(String text) {
    	Label label = new Label(text);
        label.setPrefWidth(20);
        label.setAlignment(Pos.CENTER);
        
    	return label;
    }
    
    /**
     * Creates a HBox with alignment.
     * 
     * @return HBox
     */
    private HBox buttonGroup() {
    	HBox box = new HBox();
    	box.setAlignment(Pos.CENTER_LEFT);
    	box.setPadding(new Insets(10,10,10,10));
    	box.setSpacing(10);
    	return box;
    }

    /**
     * Creates a Button for saving a file.
     * 
     * @param primaryStage Stage
     * @return Button
     */
    private Button fileSaveButton(Stage primaryStage) {
        Button fileSaveButton = new Button("Save file");
        
        fileSaveButton.setOnAction(
    		new EventHandler<ActionEvent>() {
    			@Override
    			public void handle(final ActionEvent e) {
    				try {
    					Path path = fileChooser.showSaveDialog(primaryStage).toPath();
    					savestuff(path);
    				}
    				catch(NullPointerException f) {
    					
    				}
    			}
    		});
        
        fileSaveButton.setPrefWidth(110);
        
    	return fileSaveButton;
    }
    
    /**
     * Creates a Button for loading a file.
     * 
     * @param primaryStage Stage
     * @return Button
     */
    private Button fileLoadButton(Stage primaryStage) {
    	Button fileLoadButton = new Button("Load file");
        
        fileLoadButton.setOnAction(
        	new EventHandler<ActionEvent>() {
        		@Override
        		public void handle(final ActionEvent e) {
        			try {
	        			Path path = fileChooser.showOpenDialog(primaryStage).toPath();
	        			if (path != null) {
	        				canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
	        				xAttributeChoiceBox.getItems().clear();
	        				yAttributeChoiceBox.getItems().clear();
	        				loadData(path);
	        				textField.setText(path.toString());
	        			}
        			}
        			catch(NullPointerException f) {
        				
        			}
        		}
        	});
    	
        fileLoadButton.setPrefWidth(110);
        
    	return fileLoadButton;
    }
    
    /**
     * Draws classified points.
     * 
     */
    private void drawClassified() {
    	int[] classification = kmeans.getClassification();
    	
    	if (xNormalized != null && yNormalized != null) {
	    	for (int i = 0; i < xNormalized.length; i++) {
	    		setColor(classification[i]);
	    		gc.fillOval(xNormalized[i] - 2, RANGE - yNormalized[i] - 2, 5, 5);	
	    	}
    	}
    	
    }
    
    /**
     * Draws centroids.
     * 
     */
    private void drawCentroids() {
    	double[] cx = null;
    	double[] cy = null;
		gc.setFill(Color.MAGENTA);
    	
    	if (kmeans != null) {
	    	cx = kmeans.getCentroidX();
	    	cy = kmeans.getCentroidY();
	    	
	    	for (int i = 0; i < kChoiceBox.getSelectionModel().getSelectedIndex() + 1; i++) {
	    		gc.fillOval(cx[i] - 4, RANGE - cy[i] - 4, 9, 9);
	    	}
    	}
    }
    
    /**
     * Draws non-classified points.
     * 
     */
    private void drawInitial() {
    	clearCanvas();
    	
    	if (xNormalized != null && yNormalized != null) {
	    	for (int i = 0; i < xNormalized.length; i++) {
	    		gc.setFill(Color.BLACK);
	    		gc.fillOval(xNormalized[i] - 2, RANGE - yNormalized[i] - 2, 5, 5);	
	    	}
    	}
    }
    
    /**
     * Sets color for drawing.
     * 
     * @param k cluster
     */
    private void setColor(int k) {
    	switch(k) {
	    	case 0:
	    		gc.setFill(Color.RED);
	    		break;
	    	case 1: 
	    		gc.setFill(Color.BLUE);
	    		break;
	    	case 2:
	    		gc.setFill(Color.GREEN);
	    		break;
	    	case 3:
	    		gc.setFill(Color.BROWN);
	    		break;
	    	case 4:
	    		gc.setFill(Color.ORANGE);
	    		break;
	    	case 5:
	    		gc.setFill(Color.PINK);
	    		break;
	    	case 6:
	    		gc.setFill(Color.DARKGREY);
	    		break;
	    	case 7:
	    		gc.setFill(Color.AQUA);
	    		break;
	    	case 8:
	    		gc.setFill(Color.PURPLE);
	    		break;
	    	case 9:
	    		gc.setFill(Color.YELLOW);
	    		break;
    		default:
    			gc.setFill(Color.BLACK);
    	}
    }
    
    /**
     * Clears canvas.
     */
    private void clearCanvas() {
    	canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }
    
    /**
     * Saves data in a new csv file:
     * x, x normalized, y, y normalized, classification
     * 
     * @param path File to save to.
     */
    private void savestuff(Path path) {
    	try (BufferedWriter writer = Files.newBufferedWriter(path);
    			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.RFC4180.withHeader(
    				xAttributeChoiceBox.getSelectionModel().getSelectedItem(),
    				xAttributeChoiceBox.getSelectionModel().getSelectedItem() + " Norm 0-" + RANGE,
    				yAttributeChoiceBox.getSelectionModel().getSelectedItem(),
    				yAttributeChoiceBox.getSelectionModel().getSelectedItem() + " Norm 0-" + RANGE,
    				"Classificaton k="+kChoiceBox.getSelectionModel().getSelectedItem()	))) {
    		if (kmeans != null) {
    			int[] classification = kmeans.getClassification();
    		
    			for (int i = 0; i < classification.length; i++) {
    				csvPrinter.printRecord(
    						records.get(i + 1).get(xAttributeChoiceBox.getSelectionModel().getSelectedIndex()), xNormalized[i],
    						records.get(i + 1).get(yAttributeChoiceBox.getSelectionModel().getSelectedIndex()), yNormalized[i],
    						classification[i]);
    			}
    		}
    	}
    	catch(Exception e) {
    		try {
    			Files.delete(path); // delete file in case of an error
    		}
    		catch(IOException f) {
    		}
    	}
    	
    	
    }
	   
    /**
     * Loads data from a file.
     * 
     * @param path Path to file
     */
    private void loadData(Path path) {

		try (Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path.toFile())));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.RFC4180)) {
			records = csvParser.getRecords();
			attributes = new String[records.get(0).size()];
			
			for (int i = 0; i < records.get(0).size(); i++) {
				attributes[i] = records.get(0).get(i);
				
				xAttributeChoiceBox.getItems().add(attributes[i]);
				yAttributeChoiceBox.getItems().add(attributes[i]);
			}
		} 
		catch (Exception e) {
			// failed loading, do nothing
		}
    }
    
    
    /**
     * Creates a Button for resetting and calculating new centroids.
     * 
     * @return
     */
    private Button centroidButton() {
    	Button centroidButton = new Button("Generate centroids");
    	
        centroidButton.setOnAction(
    		new EventHandler<ActionEvent>() {
    			@Override
        		public void handle(final ActionEvent e) {
    				if (kmeans != null) {
    					kmeans.newCentroids();
    					drawInitial();
    					drawCentroids();
    				}
    			}
    		});
    	
        centroidButton.setPrefWidth(250);
        
    	return centroidButton;
    }
    
    /**
     * Creates a Button for doing a round of k-means.
     * 
     * @return Button
     */
    private Button clusterButton() {
    	Button clusterButton = new Button("Cluster");
    	
        clusterButton.setOnAction(
    		new EventHandler<ActionEvent>() {
    			@Override
        		public void handle(final ActionEvent e) {
    				if (kmeans != null) {
    					clearCanvas();
    					kmeans.round();
    					drawClassified();
    					drawCentroids();
    					kmeans.calcNewCentroids();
    				}
    			}
    		});
    	
        clusterButton.setPrefSize(250, 220);
        
    	return clusterButton;
    }
    
    /**
     * Initializes k-means.
     * 
     */
    private void setupKMeans() {
    	int xIndex = xAttributeChoiceBox.getSelectionModel().getSelectedIndex();
    	int yIndex = yAttributeChoiceBox.getSelectionModel().getSelectedIndex();
    	int k = kChoiceBox.getSelectionModel().getSelectedIndex() + 1;
    	
    	if (xIndex != -1 && yIndex != -1 && k != 0) {
    		xNormalized = Normalize.normalize(records, xIndex, RANGE);
    		yNormalized = Normalize.normalize(records, yIndex, RANGE);
    		
    		kmeans = new Kmeans(xNormalized, yNormalized, k, RANGE);
    		drawInitial();
    	}
    }
    
    /**
     * Creates a ChoiceBox for the number of clusters.
     * 
     * @return ChoiceBox
     */
    private ChoiceBox<Integer> kChoiceBox() {
    	ChoiceBox<Integer> choiceBox = new ChoiceBox<Integer>();
    	
    	for (int i = 0; i < 10; i++) {
    		choiceBox.getItems().add(i + 1);
    	}
    	
        choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
        	@Override
        	public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
        		setupKMeans();
        	}
        });
    	
        choiceBox.setPrefWidth(200);
        
    	return choiceBox;
    }
    
    /**
     * Creates a ChoiceBox for attributes
     * 
     * @return ChoiceBox
     */
    private ChoiceBox<String> attributeChoiceBox() {
    	ChoiceBox<String> choiceBox = new ChoiceBox<String>();
    	
        choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
        	@Override
        	public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
        		setupKMeans();
        	}
        });
        
        choiceBox.setPrefWidth(200);
        
    	return choiceBox;
    }
}

