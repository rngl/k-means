package kmeans;

/**
 * k-means for 2 dimensional normalized data.
 * 
 * 
 * @author Antti Pasanen
 *
 */
public class Kmeans {

	private double[] table1;
	private double[] table2;
	private int[] classification;
	private double[] centroid1;
	private double[] centroid2;
	private int k;
	private int[] sizes; // number of elements in clusters
	private int range;
	
	
	/**
	 * 
	 * Constructor.
	 * 
	 * @param table1 First set of data (X)
	 * @param table2 Second set of data (Y)
	 * @param k	Number of clusters
	 * @param range Range of values, from 0 to range 
	 */
	public Kmeans(double[] table1, double[] table2, int k, int range) {
		
		this.table1 = table1;
		this.table2 = table2;
		this.k = k;
		this.range = range;
		
		if (table1 == null)
			classification = new int[0];
		else
			classification = new int[table1.length];
		
		centroid1 = new double[k];
		centroid2 = new double[k];
		sizes = new int[k];
		
		for (int i = 0; i < k; i++) {
			centroid1[i] = Math.random() * range;
			centroid2[i] = Math.random() * range;
		}
		
		for (int i = 0; i < classification.length; i++) {
			classification[i] = -1;
		}
	}

	/**
	 * Generates new random centroids.
	 * 
	 */
	public void newCentroids() {
		for (int i = 0; i < k; i++) {
			centroid1[i] = Math.random() * range;
			centroid2[i] = Math.random() * range;
		}
	}

	
	/**
	 * Classifies each point to closest centroid.
	 * 
	 */
	public void round() {
		if (table1 == null || table2 == null)
			return;
		
		int cls;
		double dist;
		double temp;
		
		for (int i = 0; i < k; i++) {
			sizes[i] = 0;
		}
		
		for (int i = 0; i < classification.length; i++) {
			cls = 0;
			dist = range * 10; 
			
			for (int j = 0; j < k; j++) {
				temp = distance(table1[i], table2[i], centroid1[j], centroid2[j]);
				
				if (temp < dist) {
					dist = temp;
					cls = j;
				}
			}
			classification[i] = cls;
			sizes[cls]++;
		}
	}
	
	
	/**
	 * Calculates distance between 2 points
	 * 
	 * @param x1 x1
	 * @param y1 y1
	 * @param x2 x2
	 * @param y2 y2
	 * @return
	 */
	private double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
	}
	

	/**
	 * Calculates new centroid values based on current classification.
	 * 
	 */
	public void calcNewCentroids() {
		for (int i = 0; i < k; i++) {
			centroid1[i] = 0;
			centroid2[i] = 0;
		}
		
		for (int i = 0; i < classification.length; i++) {
			centroid1[classification[i]] += table1[i] / sizes[classification[i]];
			centroid2[classification[i]] += table2[i] / sizes[classification[i]];
		}
	}
	
	
	/**
	 * returns classification table.
	 * 
	 * @return classification table
	 */
	public int[] getClassification() {
		return classification;
	}
	
	/**
	 * Returns first centroid table;
	 * 
	 * @return centroid table
	 */
	public double[] getCentroidX() {
		return centroid1;
	}
	
	/**
	 * Returns second centroid table.
	 * 
	 * @return centroid table
	 */
	public double[] getCentroidY() {
		return centroid2;
	}
}
