package kmeans;

import java.util.List;

import org.apache.commons.csv.CSVRecord;

public class Normalize {

	
	/**
	 * 
	 * Normalises an attribute from given records at index from 0 to range.
	 * 
	 * 
	 * @param records List<CSVRecord>
	 * @param index attribute index
	 * @param range normalization from 0 to range
	 * @return
	 */
	public static double[] normalize(List<CSVRecord> records, int index, int range) {
		
		if (records == null)
			return null;
		if (index >= records.get(0).size())
			return null;
		
		double[] normalized = new double[records.size() - 1];
		double min;
		double max;
		
		try {
			// initial values for min and max
			max = min = Double.parseDouble(records.get(records.size() - 1).get(index));
			
			for (int i = 0; i < normalized.length; i++) {
				normalized[i] = Double.parseDouble(records.get(i + 1).get(index));
				
				if (normalized[i] < min)
					min = normalized[i];
				if (normalized[i] > max)
					max = normalized[i];
			}
		}
		catch (NumberFormatException e) { // string value 
			return null;
		}
		
		// normalization formula
		for (int i = 0; i < normalized.length; i++) {
			normalized[i] = (normalized[i] - min) / (max - min) * range;
		}
		
		return normalized;
	}
	
	
	
	
	
	
}
